#ifndef FIS_H_
#define FIS_H_

#include <iostream>
#include <fstream>
#include <math.h>
#include <stdlib.h>
#include <chrono>
#include <ctime>

class FISBlock {

  public:
  // Constructor
  FISBlock(int argv0, int argv1);

  // Destructor
  ~FISBlock();

  // Controler Method
  void controler();

  private:
  int usrin0 = -1;
  int usrin1 = -1;

  // Member Variables
  double tol = 1e-8;

  int M  = -1;
  int N  = -1;
  int nz = -1;
  int m  = -1;

  double *x0, *x1, *x_str, *r0, *r0str, *b, *w, *err, *Minv;

  int *I, *J, *Ijac, *Jjac;
  double *val, *valjac;

  double *y, *g, *c, *s;

  double **v, **h, **Minv_gauss;

  int i1    = -1;
  int i2    = -1;
  int jth   = -1;
  int end_r = -1;
  int count = -1;
  double rho = 0.0;

  // Member Variables for CG:
  double *errtmp, *r0tmp, *p0, *p1, *ptmp;
  double *r1, *r1tmp, *r0strtmp;

  // Timing variables
  std::chrono::time_point<std::chrono::system_clock> m_StartTime;
  std::chrono::time_point<std::chrono::system_clock> m_EndTime;
  bool m_bRunning = false;

  // Constructor Methods
  void initGmres();
  void initCg();
  void sort();
  void csr();

  // Computation Methods
  void mv_product(double *mat, double *vec, double *result, int *range, int *index, int size);
  double dot_product(double *vec1, double *vec2, int size);
  double l2_norm(double *vec, int size);
  void get_krylov(double *mat1, double **mat2, double **math, double *vecw, int size);
  void rinv_g(double **mat, double *vec, double *rvec, int size);
  void forward(double **mat, double *vec, double *rvec, int size);

  // Solver methods
  void gmres();
  void cg();

  // Timing methods
  void start();
  void stop();
  double elapsedMilliseconds();
  double elapsedSeconds();

  // Destructor Method
  void finalizeGmres();
  void finalizeCg();

};

/******************/
/* TIMING METHODS */
/******************/

void FISBlock::start() {
  m_StartTime = std::chrono::system_clock::now();
  m_bRunning = true;
}

void FISBlock::stop() {
  m_EndTime = std::chrono::system_clock::now();
  m_bRunning = false;
}

double FISBlock::elapsedMilliseconds() {
  std::chrono::time_point<std::chrono::system_clock> endTime;
  if(m_bRunning) {
    endTime = std::chrono::system_clock::now();
  } else {
    endTime = m_EndTime;
  }
  return std::chrono::duration_cast<std::chrono::milliseconds>(endTime - m_StartTime).count();
}

double FISBlock::elapsedSeconds() {
  return elapsedMilliseconds() / 1000.0;
}

#endif // FIS_H_
