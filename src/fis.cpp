#include "fis.h"

using namespace std;

// Constructor
FISBlock::FISBlock(int argv0, int argv1) {
  usrin0 = argv0;
  usrin1 = argv1;
  if (usrin0 == 1) {
    initGmres();
  } else {
    initCg();
  }
  sort();
  csr();
}

// Destructor
FISBlock::~FISBlock() {
    stop();

    if (usrin0 == 1) {
      finalizeGmres();
    } else {
      finalizeCg();
    }
    cout << "Runtime: " << elapsedSeconds() << " (seconds)" << endl;
}

void FISBlock::initGmres() {
  ifstream mtx;

  mtx.open("../Matrices/orsirr_1.dat");

  mtx >> M;
  mtx >> N;
  mtx >> nz;

  if(usrin1 > 0 && usrin1 != 2) {
    m = M;
  } else {
    cout << "Enter the chosen Krylov Space dimension (m): ";
    cin >> m;
  }

  x0 = new double[M+1];
  x1 = new double[M+1];
  x_str = new double[M+1];
  r0 = new double[M+1];
  r0str = new double[M+1];
  b = new double[M+1];
  w = new double[M+1];
  err = new double[M+1];
  Minv = new double[M+1];

  I = new int[nz+1];
  J = new int[nz+1];
  Ijac = new int[nz+1];
  Jjac = new int[nz+1];
  val = new double[nz+1];
  valjac = new double[nz+1];

  y = new double[m+1];
  g = new double[m+1];
  c = new double[m+1];
  s = new double[m+1];

  v = new double*[m+2];
  for(int i=0; i<m+2; i++) {
    v[i] = new double[M+1];
  }

  h = new double*[m+2];
  for(int i=0; i<m+2; i++) {
    h[i] = new double[m+1];
  }

  Minv_gauss = new double*[M+1];
  for(int i=0; i<M+1; i++) {
    Minv_gauss[i] = new double[M+1];
  }

  for(int i=1; i<=nz; i++) {
    mtx >> I[i];
    mtx >> J[i];
    mtx >> val[i];
    Ijac[i] = I[i];
    Jjac[i] = J[i];
    valjac[i] = val[i];
  }

  for(int i=1; i<=M; i++) {
    x_str[i] = 1;
    x0[i] = 0;
  }
}

void FISBlock::initCg() {
  ifstream mtx;
  mtx.open("../Matrices/s3rmt3m3.dat");

  mtx >> M;
  mtx >> N;
  mtx >> nz;

  val = new double[nz+1];
  I = new int[nz+1];
  J = new int[nz+1];

  x0 = new double[M+1];
  x1 = new double[M+1];
  x_str = new double[M+1];
  r0 = new double[M+1];
  r1 = new double[M+1];
  r1tmp = new double[M+1];
  r0str = new double[M+1];
  r0strtmp = new double[M+1];
  r0tmp = new double[M+1];
  p0 = new double[M+1];
  p1 = new double[M+1];
  ptmp = new double[M+1];
  b = new double[M+1];
  err = new double[M+1];
  errtmp = new double[M+1];

  for(int i=1; i<=nz; i++) {
    mtx >> I[i];
    mtx >> J[i];
    mtx >> val[i];
  }

  for(int i=1; i<=M; i++) {
    x_str[i] = 1;
    x0[i] = 0;
  }
}

void FISBlock::sort() {
  int itmp0 = 0;
  int itmp1 = 0;
  double dtmp = 0.0;

  for(int i=1; i<=nz; i++) {
    int min = i;

    for(int j=i; j<=nz; j++) {
        if(I[min] > I[j]) {
          min = j;
        }
    }

    itmp0 = I[i];
    itmp1 = J[i];
    dtmp  = val[i];

    I[i]   = I[min];
    J[i]   = J[min];
    val[i] = val[min];

    I[min]   = itmp0;
    J[min]   = itmp1;
    val[min] = dtmp;
  }
}

void FISBlock::csr() {
  int itmp0 = 1;
  int itmp1 = 1;

  for(int i=1; i<=nz; i++, itmp1++) {
    if(I[i] > itmp0) {
      itmp0 = I[i];
      I[itmp0] = itmp1;
    }
  }
  I[itmp0 + 1] = itmp1;
}

void FISBlock::mv_product(double *mat, double *vec, double *result,
                                       int *range, int *index, int size) {
  if (usrin0 == 1) {
    double dtmp = 0.0;
    for(int i=1; i<=size; i++) {
      i1 = range[i];
      i2 = range[i + 1] - 1;
      dtmp = 0.0;
      for(int k = i1; k <= i2 ; k++) {
        dtmp += mat[k] * vec[index[k]];
      }
      result[i] = dtmp;
    }
  } else if (usrin0 == 2) {
    for(int i=1; i<=size; i++) {
      result[i] = 0;
    }
    for(int i=1; i<=size; i++) {
      i1 = range[i];
      i2 = range[i + 1] - 1;

      for(int j = i1; j <= i2 ; j++) {
	      result[i] = result[i] + mat[j] * vec[J[j]];
	      if(i != J[j]) {
	        result[J[j]] = result[J[j]] + mat[j] * vec[i];
	      }
      }
    }
  }

}

double FISBlock::dot_product(double *vec1, double *vec2, int size) {
  double dtmp = 0.0;

  for(int i=0; i<=size; i++) {
    dtmp += vec1[i]*vec2[i];
  }

  return dtmp;
}

double FISBlock::l2_norm(double *vec, int size) {
  double dtmp = 0.0;

  for(int i=1; i<=size; i++) {
    dtmp += pow(vec[i], 2);
  }

  return sqrt(dtmp);
}

void FISBlock::get_krylov(double *mat1, double **mat2, double **math, double *vecw, int size) {
  mv_product(mat1, mat2[size], vecw, I, J, M);

  if(usrin1 == 3) {
    for(int i=1; i<=M; i++) {
	    vecw[i] = Minv[i]*vecw[i];
    }
  } else if(usrin1 == 4) {
    forward(Minv_gauss, vecw, Minv, M);
    for(int i=1; i<=M; i++) {
	    vecw[i] = Minv[i];
    }
  }

  for(int i=1; i<=size; i++) {
    math[i][size] = dot_product(mat2[i],vecw,M);
    for(int k=1; k<=M; k++) {
      vecw[k] = ( vecw[k] - (math[i][size] * mat2[i][k]) );
    }
  }

  math[size + 1][size]= l2_norm(vecw,M);

  for(int k=1; k<=M; k++) {
    mat2[size + 1][k] = vecw[k] / math[size + 1][size];
  }
}

void FISBlock::rinv_g(double **mat, double *vec, double *rvec, int size) {
  double dtmp = 0.0;

  rvec[size] = vec[size] / mat[size][size];

  for(int i=size-1; i>=1; i--) {
    dtmp=0.0;
    for(int j=size; j>i; j--) {
	    dtmp +=mat[i][j] * rvec[j];
    }
    rvec[i] = (vec[i] - dtmp) / mat[i][i];
  }
}

void FISBlock::forward(double **mat, double *vec, double *rvec, int size) {
  double dtmp = 0.0;
  rvec[1] = vec[1] / mat[1][1];

  for(int i=2; i<=size; i++) {
    dtmp=0.0;
    for(int j=1; j<i; j++) {
	    dtmp += mat[i][j] * rvec[j];
    }
    rvec[i] = (vec[i] - dtmp) / mat[i][i];
  }
}

void FISBlock::gmres() {
  int itmp0 = 0;
  count = 0;

  mv_product(val,x_str,b,I,J,M);
  mv_product(val,x0,r0str,I,J,M);
  for(int i=1; i<=M; i++) {
    r0str[i] = b[i] - r0str[i];
  }

  if (usrin1 == 3) {
    for(int i=1; i<=nz; i++) {
      if(Ijac[i] == Jjac[i])
	      Minv[J[i]] = valjac[i];
      }
    for(int i=1; i<=M; i++) {
      Minv[i] = 1.0 / Minv[i];
    }
  } else if (usrin1 == 4) {
      for(int i=1; i<=nz; i++) {
        if(Ijac[i] == Jjac[i]) {
	        Minv_gauss[Ijac[i]][Ijac[i]] = val[i];
        } else if(Ijac[i] > Jjac[i]) {
	        Minv_gauss[Ijac[i]][Jjac[i]] = val[i];
        }
      }
  }

  ofstream residual;
  ofstream error;
  residual.open("../Output/residualGmres.dat");
  error.open("../Output/errorGmres.dat");

  do {
    double dtmp1;

    if (usrin1 == 4) {
      mv_product(val, x0, Minv, I, J, M);
    } else {
      mv_product(val, x0, r0, I, J, M);
    }

    if (usrin1 == 1 || usrin1 == 2) {
      for(int i=1; i<=M; i++) {
        r0[i] = b[i] - r0[i];
      }
    } else if (usrin1 == 3) {
        for(int i=1; i<=M; i++) {
          r0[i] = Minv[i]*(b[i] - r0[i]);
        }
    } else if (usrin1 == 4) {
        for(int i=1; i<=M; i++) {
          Minv[i] = b[i] - Minv[i];
        }
        forward(Minv_gauss, Minv, r0, M);
    }

  for(int i=1; i<=M; i++) {
    v[1][i] = r0[i] / l2_norm(r0, M);
  }

  g[1] = l2_norm(r0, M);

  for(int j=1; j<=m; j++) {
    get_krylov(val, v, h, w, j);

    for(int k=2; k<=j; k++) {
      double tmp1 = h[k - 1][j];
      double tmp2 = h[k][j];
      h[k - 1][j] = c[k - 1]*tmp1 + s[k - 1]*tmp2;
      h[k][j] = -s[k - 1]*tmp1 + c[k - 1]*tmp2;
    }

    c[j] = h[j][j] / (sqrt(pow(h[j][j], 2) + pow(h[j + 1][j], 2) ));
    s[j] = h[j + 1][j] / (sqrt(pow(h[j][j], 2) + pow(h[j + 1][j], 2) ));
    h[j][j] = c[j] * h[j][j] + s[j] * h[j + 1][j];
    g[j + 1] = -s[j] * g[j];
    g[j] = c[j] * g[j];

    jth = j;
    count++;

    if (usrin1 == 1 || usrin1 == 2) {
      rho = fabs(g[j + 1]) / l2_norm(r0str, M);
      residual << count << " " << rho << "\n";
    } else if (usrin1 == 3 || usrin1 == 4) {
      rho = fabs(g[j + 1]) / l2_norm(r0, M);
      residual << j << " " << (fabs(g[j + 1])) << "\n";
    }

    rinv_g(h, g, y, jth);

    for(int i=1; i<=M; i++) {
      dtmp1 = 0;
      for(int k=1; k<=j; k++) {
        dtmp1 = dtmp1 + y[k] * v[k][i];
      }
      x1[i] = dtmp1 + x0[i];
    }

    for(int i=1; i<=M; i++) {
      err[i] = fabs(x_str[i] - x1[i]);
    }

    if (usrin1 == 1 || usrin1 == 2) {
      error << count<< " " << l2_norm(err, M) << "\n";
    } else if (usrin1 == 3) {
      error << j << " " << l2_norm(err, M) << "\n";
    }

    if(rho < tol)
      break;
    }

    // Close files for Jacobi and Gauss Pre
    if (usrin1 == 3 || usrin1 == 4) {
      residual.close();
      error.close();
    }

    rinv_g(h, g, y, jth);

    for(int i=1; i<=M; i++) {
      dtmp1 = 0;
      for(int j=1; j<=jth; j++) {
        dtmp1 = dtmp1 + y[j] * v[j][i];
      }
      x1[i] = dtmp1 + x0[i];
    }

  // Break if not restarted
  if (usrin1 != 2) {
    break;
  }

  // For Restarted GMRES
  for(int i=1; i<=M; i++) {
    x0[i] = x1[i];
  }
  itmp0++;
  residual << itmp0 << " " << rho << "\n";

  } while(rho > tol);

  if (usrin1 == 1 || usrin1 == 2) {
    residual.close();
    error.close();
  }
}

void FISBlock::cg() {
  mv_product(val, x_str, b, I, J, M);

  count = 0;
  double alpha, beta;

  mv_product(val, x0, r0tmp, I, J, M);

  for(int i=1; i<=M; i++) {
    r0[i] = b[i] - r0tmp[i];
  }

  for(int i=1; i<=M; i++) {
    p0[i] = r0[i];
    r0str[i] = r0[i];
  }

  ofstream residual;
  ofstream error;
  residual.open("../Output/residualCg.dat");
  error.open("../Output/errorCg.dat");

  do {
    mv_product(val, p0, ptmp, I, J, M);

    alpha = dot_product(r0, r0, M) / dot_product(ptmp, p0, M);

    for(int i=1; i<=M; i++) {
	    x1[i] = x0[i] + alpha * p0[i];
	    r1[i] = r0[i] - alpha * ptmp[i];
    }

    beta = dot_product(r1, r1, M) / dot_product(r0, r0, M);

    for(int i=1; i<=M; i++) {
	    p1[i] = r1[i] + beta * p0[i];
    }

    for(int i=1; i<=M; i++) {
	    x0[i] = x1[i];
	    r0[i] = r1[i];
	    p0[i] = p1[i];
    }
    count++;

    //mv_product(val, r1, r1tmp, I, J, M);
    //mv_product(val, r0str, r0strtmp, I, J, M);

    // A-norm
    //residual << count << " " << (l2_norm(r1, M) / l2_norm(r0str, M)) << "\n";
    residual << count << " " << (l2_norm(r1, M)) <<"\n";

    for(int i=1; i<=M; i++) {
	    err[i] = fabs(x_str[i] - x1[i]);
    }

    //mv_product(val, err, errtmp, I, J, M);

    error << count << " " << l2_norm(err, M) << "\n";

  }while((l2_norm(r1, M) / l2_norm(r0str, M)) > tol);

  residual.close();
  error.close();
}

void FISBlock::finalizeGmres() {
  delete [] x0;
  delete [] x1;
  delete [] x_str;
  delete [] r0;
  delete [] r0str;
  delete [] b;
  delete [] w;
  delete [] err;
  delete [] Minv;

  delete [] I;
  delete [] J;
  delete [] Ijac;
  delete [] Jjac;
  delete [] val;
  delete [] valjac;

  delete [] y;
  delete [] g;
  if (usrin1 != 2) {
    delete [] c;
  }
  delete [] s;

  for(int i=0; i<m+2; i++) {
    delete [] v[i];
  }
  delete [] v;

  for(int i=0; i<m+2; i++) {
    delete [] h[i];
  }
  delete [] h;

  for(int i=0; i<M+1; i++) {
    delete [] Minv_gauss[i];
  }
  delete [] Minv_gauss;
}

void FISBlock::finalizeCg() {
  delete [] val;
  delete [] I;
  delete [] J;

  delete [] x_str;
  delete [] x0;
  delete [] x1;
  delete [] b;
  delete [] r0str;
  delete [] r0;
  delete [] r1;
  delete [] r0tmp;
  delete [] p0;
  delete [] p1;
  delete [] ptmp;
}

void FISBlock::controler() {
  start();
  if (usrin0 == 1) {
    gmres();
  } else {
    cg();
  }
}

int main() {
  int usrin0 = -1;
  int usrin1 = -1;
  bool flag = true;
  do {
    cout << "Choose a method:\n"
         << "1.GMRES\n"
         << "2.CG\n"
         << "Input: ";
    cin >> usrin0;
    cout << endl;

    if (usrin0 == 1 || usrin0 == 2) {
       flag = false;
    } else {
       cout << "WARNING: Not among the input options.\n"
            << "Please ";
    }

  } while(flag);

  if (usrin0 == 1 ) {
    flag = true;
    do {
      cout << "Now choose the GMRES method type:\n"
           << "1.Full GMRES\n"
           << "2.Restarted GMRES\n"
           << "3.Jacobi Preconditioning\n"
           << "4.Gauss-Seidel Preconditioning\n"
           << "Input: ";
      cin >> usrin1;
      cout << endl;

      if (usrin1 >= 1 && usrin1 <= 4) {
        flag = false;
      } else {
        cout << "WARNING: Not among the input options.\n"
             << "Please ";
      }

    } while(flag);
  }

  FISBlock M(usrin0, usrin1);
  M.controler();
}
