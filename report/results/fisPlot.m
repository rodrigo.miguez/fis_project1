clear all, close all

clc

residual = load('residualGmresRestart_m50.dat');

tableSize = length(residual(:,1));

residualNew = [];
for i = 1:tableSize-1

    if ( (i ~= 1) && ( residual(i,1) ~= (residual(i-1,1) +1 ) ) && (i < length(residual(:,1))) )
        residualNew(residual(i+1,1),1) = residual(i+1,1);
        residualNew(residual(i+1,1),2) = residual(i+1,2);
        continue
    end
      
    residualNew(residual(i,1),1) = residual(i,1);
    residualNew(residual(i,1),2) = residual(i,2);
    
end

semilogy(residualNew(:,1), residualNew(:,2), '-b');
ylabel('Residual Norm'), xlabel('Iterations');
grid on;

