clear all, close all

clc

residual = load('residualGmres.dat');

figure (1)
semilogy(residual(:,1), residual(:,2), '-b');

ylabel('Residual Norm'), xlabel('Iterations');
grid on;

