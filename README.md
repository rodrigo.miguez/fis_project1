FIS Project 1 - Krylov Methods

Directory structure:
 * /matrix: matrices used to run the code
 * /mmio: files provided by MatrixMarket to read and write the matrices
 * /output: folder that the outputs of the code writes the output data
 * /report: report folder with images and data files
 * /src: source files folder. To run the code: ./run
